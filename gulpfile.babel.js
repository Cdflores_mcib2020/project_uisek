import del from 'del';
import { task, src, dest, series, lastRun, watch, parallel } from 'gulp';
import { init, write } from 'gulp-sourcemaps';
import babel from 'gulp-babel';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import minifyCss from 'gulp-clean-css';
import plumber from 'gulp-plumber';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import handlebars_compile from 'gulp-compile-handlebars';
import imagemin from 'gulp-imagemin';
import rename from 'gulp-rename';
import webpack from 'webpack-stream';
import { create } from 'browser-sync';
import layouts from 'handlebars-layouts';
import htmlmin from 'gulp-htmlmin';

import helpers from './handlebars_helpers';

const src_folder = './src/';
const src_assets_templates = src_folder + 'templates/';
const src_assets_pages = src_folder + 'pages/';
const src_assets_folder = src_folder + 'assets/';
const dist_folder = './dist/';
const dist_assets_folder = dist_folder + 'assets/';
const dist_assets_pages = dist_folder + 'pages/';
const node_modules_folder = './node_modules/';
const dist_node_modules_folder = dist_folder + 'node_modules/';
const node_dependencies = Object.keys(require('./package.json').dependencies || {});
const browserSync = create();

handlebars_compile.Handlebars.registerHelper(layouts(handlebars_compile.Handlebars));

task('clear', () => del([dist_folder]));

task('handlebars-templates', () => {
	const templateData = {};
	const options = {
		ignorePartials: true,
		batch: [src_assets_templates],
		helpers: {
			capitals: str => {
				return str.toUpperCase();
			},
			...helpers,
		},
	};

	src([src_assets_pages + '**/*.hbs'])
		.pipe(handlebars_compile(templateData, options))
		.pipe(
			rename(path => {
				path.extname = '.html';
			}),
		)
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(dest(dist_assets_pages))
		.pipe(browserSync.stream());

	return src(src_folder + 'index.hbs')
		.pipe(handlebars_compile(templateData, options))
		.pipe(
			rename({
				extname: '.html',
			}),
		)
		.pipe(dest(dist_folder))
		.pipe(browserSync.stream());
});

task('js', () => {
	return src([src_assets_folder + 'js/**/*.js'], { since: lastRun('js') })
		.pipe(plumber())
		.pipe(
			webpack({
				mode: 'production',
			}),
		)
		.pipe(init())
		.pipe(
			babel({
				presets: ['@babel/env'],
			}),
		)
		.pipe(concat('all.js'))
		.pipe(uglify())
		.pipe(write('.'))
		.pipe(dest(dist_assets_folder + 'js'))
		.pipe(browserSync.stream());
});

task('sass', () => {
	return src([src_assets_folder + 'sass/**/*.sass'], { since: lastRun('sass') })
		.pipe(init())
		.pipe(plumber())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(minifyCss())
		.pipe(write('.'))
		.pipe(dest(dist_assets_folder + 'css'))
		.pipe(browserSync.stream());
});

task('scss', () => {
	return src([src_assets_folder + 'scss/**/*.scss'], { since: lastRun('scss') })
		.pipe(init())
		.pipe(plumber())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(minifyCss())
		.pipe(write('.'))
		.pipe(dest(dist_assets_folder + 'css'))
		.pipe(browserSync.stream());
});

task('images', () => {
	return src([src_assets_folder + 'images/**/*.+(png|jpg|jpeg|gif|svg|ico)'], { since: lastRun('images') })
		.pipe(plumber())
		.pipe(imagemin())
		.pipe(dest(dist_assets_folder + 'images'))
		.pipe(browserSync.stream());
});

task('vendor', () => {
	if (node_dependencies.length === 0) {
		return new Promise(resolve => {
			console.log('No dependencies specified');
			resolve();
		});
	}

	return src(
		node_dependencies.map(dependency => node_modules_folder + dependency + '/**/*.*'),
		{
			base: node_modules_folder,
			since: lastRun('vendor'),
		},
	)
		.pipe(dest(dist_node_modules_folder))
		.pipe(browserSync.stream());
});

// Watch
task('watch', () => {
	const watch_images = src_assets_folder + 'images/**/*.+(png|jpg|jpeg|gif|svg|ico)';
	const watch_vendor = [];

	node_dependencies.forEach(dependency => {
		watch_vendor.push(node_modules_folder + dependency + '/**/*.*');
	});

	const files_watch = [
		src_folder + '**/*.hbs',
		src_assets_templates + '**/*.hbs',
		src_assets_pages + '**/*.hbs',
		src_assets_folder + 'sass/**/*.sass',
		src_assets_folder + 'scss/**/*.scss',
		src_assets_folder + 'js/**/*.js',
	];

	watch(files_watch, series('dev')).on('change', browserSync.reload);
	watch(watch_images, series('images')).on('change', browserSync.reload);
	watch(watch_vendor, series('vendor')).on('change', browserSync.reload);
});

// Serve
task('serve', () => {
	return browserSync.init({
		server: {
			baseDir: ['dist'],
		},
		port: 3000,
		open: false,
	});
});

// Dev
task('dev', series('handlebars-templates', 'js', 'sass', 'scss'));

// Build
task('build', series('clear', 'dev', 'images', 'vendor'));

// Default task
task('default', series('build', parallel('serve', 'watch')));
