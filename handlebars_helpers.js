
function link(path, label, target, classess) {
	if (path) {
		path = path === '/' ? path : `${path}.html`;
		label = typeof label === 'string' ? label : path;
		target = typeof target === 'string' ? target : '';
		classess = typeof classess === 'string' ? classess : '';
		return `<a class="${classess}" target="${target}" href="${path}">${label}</a>`;
	} else {
		return '';
	}
}


export default { link };
